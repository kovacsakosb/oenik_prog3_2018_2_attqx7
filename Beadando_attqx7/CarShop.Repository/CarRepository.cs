﻿using CarShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarShop.Repository
{
    public class CarRepository:ICarRepository
    {
        CarShopEntities database;
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="database">the sql database</param>
        public CarRepository(CarShopEntities database)
        {
            this.database = database;
        }
        /// <summary>
        /// List all entity from a talbe
        /// </summary>
        /// <param name="obj">the table</param>
        /// <returns>string contains all entity values fromed</returns>
        public string List(object obj)
        {
            string back = "";
            if (obj is AutoMarkak)
            {
                foreach (AutoMarkak item in database.AutoMarkak)
                {
                    back += item + "\n";
                }
            }
            if(obj is Extrak)
            {
                foreach (Extrak item in database.Extrak)
                {
                    back += item + "\n";
                }
            }
            if (obj is Modellek)
            {
                foreach (Modellek item in database.Modellek)
                {
                    back += item + "\n";
                }
            }
            return back;
        }

        /// <summary>
        /// delete an entity from Automarkak table by id and set null the ModellExtraKapcsolo marka_id and set null the Modellek marka_id 
        /// </summary>
        /// <param name="id">the deleted entity id</param>
        public void DeleteAuto(int id)
        {
            if(this.database.AutoMarkak.Count(x => x.Id == id)==0)
            {
                Console.WriteLine("Nincs ilyen id");
            }
            else
            {
                AutoMarkak auto = this.database.AutoMarkak.Single(x => x.Id == id);
                this.database.AutoMarkak.Remove(auto);
                List<Modellek> listModel = this.database.Modellek.Where(x => x.Marka_Id == id).ToList();
                List<ModellExtraKapcsolo> listexModel = this.database.ModellExtraKapcsolo.Where(x => x.Marka_Id == id).ToList();

                foreach (Modellek item in listModel)
                {

                    item.Marka_Id = null;

                }
                foreach (ModellExtraKapcsolo item2 in listexModel)
                {

                    item2.Marka_Id = null;

                }
                this.database.SaveChanges();
            }
            
        }
        /// <summary>
        /// delete an entity from Extrak table by id and set null the ModellExtraKapcsolo extra_id
        /// </summary>
        /// <param name="id">the deleted entity id</param>
        public void DeleteExtra(int id)
        {
            if(this.database.Extrak.Count(x => x.Id == id) == 0)
            {
                Console.WriteLine("Nincs ilyen id");
            }else
            {
                database.Extrak.Remove(database.Extrak.Single(x => x.Id == id));
                List<ModellExtraKapcsolo> listexModel = this.database.ModellExtraKapcsolo.Where(x => x.Extra_Id == id).ToList();

                foreach (ModellExtraKapcsolo item in listexModel)
                {

                    item.Extra_Id = null;

                }
                database.SaveChanges();
            }
            
        }
        /// <summary>
        /// delete an entity from Modellek table by id
        /// </summary>
        /// <param name="id">the deleted entity id</param>
        public void DeleteModel(int id)
        {
            if(this.database.Modellek.Count(x => x.Id == id) == 0)
            {
                Console.WriteLine("Nincs ilyen id");
            }else
            {
                database.Modellek.Remove(database.Modellek.Single(x => x.Id == id));
                database.SaveChanges();
            }
            
        }
        /// <summary>
        /// Update and entity in automarkak table by id
        /// </summary>
        /// <param name="id">the updated entity id</param>
        /// <param name="propertyNumber">the number of property</param>
        /// <param name="newPropertyValue">the new value of property</param>
        public void UpdateAuto(int id,int propertyNumber,string newPropertyValue)
        {
            if (database.AutoMarkak.Count(x => x.Id == id) != 0)
            {
                AutoMarkak auto = database.AutoMarkak.Single(x => x.Id == id);
                switch (propertyNumber)
                {

                    case 2:
                        auto.nev = newPropertyValue;
                        break;
                    case 3:
                        auto.Orszag_nev = newPropertyValue;
                        break;
                    case 4:
                        auto.url = newPropertyValue;
                        break;
                    case 5:
                        auto.Alapitas_eve = int.Parse(newPropertyValue);

                        break;
                    case 6:
                        auto.Eves_forgalom = int.Parse(newPropertyValue);
                        break;
                    default:
                        Console.WriteLine("Rossz adat");
                        break;
                }
                database.SaveChanges();
            }
            else
            {
                Console.WriteLine("Nincs ilyen id");
            }
        }
        /// <summary>
        /// Update and entity in extrak table by id
        /// </summary>
        /// <param name="id">the updated entity id</param>
        /// <param name="propertyNumber">the number of property</param>
        /// <param name="newPropertyValue">the new value of property</param>
        public void UpdateExtra(int id, int propertyNumber, string newPropertyValue)
        {
            if (database.Extrak.Count(x => x.Id == id) != 0)
            {
                Extrak extra = database.Extrak.Single(x => x.Id == id);
                switch (propertyNumber)
                {

                    case 2:
                        extra.Kategoria_nev = newPropertyValue;
                        break;
                    case 3:
                        extra.Nev = newPropertyValue;
                        break;
                    case 4:
                        extra.Ar = int.Parse(newPropertyValue);
                        break;
                    case 5:
                        extra.Szin = newPropertyValue;

                        break;
                    case 6:
                        extra.Garancia = bool.Parse(newPropertyValue);
                        break;
                    default:
                        Console.WriteLine("Rossz adat");
                        break;
                }
                database.SaveChanges();
            }
            else
            {
                Console.WriteLine("Nincs ilyen id");
            }
        }
        /// <summary>
        /// Update and entity in Modellek table by id
        /// </summary>
        /// <param name="id">the updated entity id</param>
        /// <param name="propertyNumber">the number of property</param>
        /// <param name="newPropertyValue">the new value of property</param>
        public void UpdateModel(int id, int propertyNumber, string newPropertyValue)
        {
            if (database.Modellek.Count(x => x.Id == id) != 0)
            {
                Modellek model = database.Modellek.Single(x => x.Id == id);
                switch (propertyNumber)
                {

                    case 2:
                        model.Marka_Id = int.Parse(newPropertyValue);
                        break;
                    case 3:
                        model.Nev = newPropertyValue;
                        break;
                    case 4:
                        model.Megjelenes_eve = int.Parse(newPropertyValue);
                        break;
                    case 5:
                        model.Motorterfogat = int.Parse(newPropertyValue);

                        break;
                    case 6:
                        model.Loero = int.Parse(newPropertyValue);
                        break;
                    case 7:
                        model.Alapar = int.Parse(newPropertyValue);
                        break;
                    default:
                        Console.WriteLine("Rossz adat");
                        break;
                }
                database.SaveChanges();
            }
            else
            {
                Console.WriteLine("nincs ilyen Id");
            }
        }

        
        /// <summary>
        /// Add an entity to the right table
        /// </summary>
        /// <param name="obj">the new entity</param>
        public void Add(object obj)
        {
            
            
            if (obj is AutoMarkak)
            {
                int id = (obj as AutoMarkak).Id;
                if (database.AutoMarkak.Count(x=>x.Id==id)==0)
                {
                    database.AutoMarkak.Add((AutoMarkak)obj);
                    database.SaveChanges();
                    Console.WriteLine((AutoMarkak)obj + " hozzáadva.");
                }
                else
                {
                    Console.WriteLine("Már van ilyen id");
                }
                
            }
            if (obj is Extrak)
            {
                int id = (obj as Extrak).Id;
                if (database.Extrak.Count(x => x.Id == id) == 0)
                {
                    database.Extrak.Add((Extrak)obj);
                    database.SaveChanges();
                    Console.WriteLine((Extrak)obj + " hozzáadva.");
                }else
                {
                    Console.WriteLine("Már van ilyen id");
                }
            }
            if (obj is Modellek)
            {
                int id = (obj as Modellek).Id;
                if (database.Modellek.Count(x => x.Id == id) == 0)
                {
                    database.Modellek.Add((Modellek)obj);
                    database.SaveChanges();
                    Console.WriteLine((Modellek)obj + " hozzáadva.");
                }else
                {
                    Console.WriteLine("Már van ilyen modell");
                }
            }
        }
        /// <summary>
        /// listing entities by minimum horsepower
        /// </summary>
        /// <param name="horsepower">Minimum horsepower</param>
        /// <returns>return all entity in a string</returns>
        public string ListHorse(int horsepower)
         {
            string back = "";
            List<Modellek> horsepowermodels = database.Modellek.Where(x => x.Loero > horsepower).ToList();

            foreach (Modellek item in horsepowermodels)
            {
                AutoMarkak auto = database.AutoMarkak.Single(x => x.Id == item.Marka_Id);
                back +=" Neve: "+ auto.nev + " " + item.Nev + " Lóerő: " + item.Loero + " Megjelenés Éve: " + item.Megjelenes_eve + " Alapára: " + item.Alapar + "\n";
            }
            if(back=="")
            {
                return "Nincs ilyen auto";
            }else
            {
                return back;
            }
            
         }
        /// <summary>
        /// list all car with the extra
        /// </summary>
        /// <param name="extrasString">extra name</param>
        /// <returns>return all entity in a string</returns>
        public string ExtraFilter(string extrasString)
        {
            string back = "";
            if(database.Extrak.Count(x => x.Nev == extrasString)!=0)
            {
                int extraid = database.Extrak.Single(x => x.Nev == extrasString).Id;
                List<ModellExtraKapcsolo> autoids = database.ModellExtraKapcsolo.Where(x => x.Extra_Id == extraid).ToList();
                List<AutoMarkak> autok = database.AutoMarkak.ToList();
                foreach (ModellExtraKapcsolo item in autoids)
                {
                    var currentid = item.Marka_Id;
                    foreach (AutoMarkak itemtwo in autok)
                    {
                        if (itemtwo.Id == currentid)
                            back += itemtwo.ToString() + "\n";
                    }

                }
                return back;
            }
            else
            {
                return "Nincs ilyen extra";
            }
            

            //if(back=="")
            //{
            //    return "Nincs ilyen extra";
            //}else
            //{
            //    return back;
            //}
            
        }
        //public string ExtraFilter(string extrasString)
        //{
        //    string[] extrasArray = extrasString.Split(';');
        //    int[] extraid = new int[extrasArray.Length];
        //    Array.Sort(extraid);
        //    int index = 0;
        //    foreach (string item in extrasArray)
        //    {
        //        Extrak ex = database.Extrak.Single(x => x.Nev.ToUpper() == item.ToUpper());
        //        extraid[index] = ex.Id;
        //        index++;
        //    }
        //    int i = 0;

        //    List<ModellExtraKapcsolo> extramodel = database.ModellExtraKapcsolo.OrderBy(x=>x.Marka_Id).OrderBy(x=>x.Extra_Id).ToList();
        //    foreach (ModellExtraKapcsolo item in extramodel)
        //    {



        //    }

        //    return "";
        //}
        //private bool Containsextra(ModellExtraKapcsolo model,int extraid)
        //{
        //    return model.Extra_Id == extraid;
        //}
        /// <summary>
        /// list all model from choosen brand
        /// </summary>
        /// <param name="brand">the barnd name</param>
        /// <returns>return all entity in a string</returns>
        public string BrandList(string brand)
        {
            string toconsolole = "";
            if(database.AutoMarkak.Count(x => x.nev.ToUpper() == brand.ToUpper())==0)
            {
                return "Nincs Ilyen márka";
            }else
            {
                AutoMarkak auto = database.AutoMarkak.Single(x => x.nev.ToUpper() == brand.ToUpper());
                List<Modellek> models = database.Modellek.ToList();
                foreach (Modellek item in models)
                {
                    if (item.Marka_Id == auto.Id)
                    {
                        toconsolole += item.ToString() + "\n";
                    }
                }
                return toconsolole;
            }
            //if(toconsolole == "")
            //{
            //    return "Nincs ilyen márka";
            //}else
            //{
            //    return toconsolole;
            //}
            
        }
    }
    public interface ICarRepository
    {
        string List(object obj);
        void DeleteAuto(int id);
        void DeleteExtra(int id);
        void DeleteModel(int id);
        void UpdateAuto(int id, int propertyNumber, string newPropertyValue);
        void UpdateExtra(int id, int propertyNumber, string newPropertyValue);
        void UpdateModel(int id, int propertyNumber, string newPropertyValue);
        void Add(object obj);
        string ListHorse(int horsepower);
        string ExtraFilter(string extrasString);
        string BrandList(string brand);
    }
}
