var indexSectionsWithContent =
{
  0: "abcdefgiklmnoprstu",
  1: "abceimpst",
  2: "c",
  3: "abcempst",
  4: "abcdefgilmnorstu",
  5: "u",
  6: "iu",
  7: "abcdegiklmnorstu",
  8: "aem",
  9: "cn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "events",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Events",
  9: "Pages"
};

