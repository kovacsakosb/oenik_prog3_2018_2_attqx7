﻿using CarShop.Data;
using CarShop.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CarShop.Program
{
    class Program
    {
        
        static void Main(string[] args)
        {
            

            CarShopEntities con = new CarShopEntities();
            BusinessLogic logic = new BusinessLogic(con);

            bool programRunnin=true;
            do
            {
                
                //System.Threading.Thread.Sleep(300);
                Console.Clear();
                Console.WriteLine("             CARSHOP MENU            ");
                Console.WriteLine("Rendelkezésre álló műveletek: ");
                Console.WriteLine(" [1], Listázás");
                Console.WriteLine(" [2], Módosítás");
                Console.WriteLine(" [3], Törlés");
                Console.WriteLine(" [4], Hozzáadás");
                Console.WriteLine(" [5], Extra funkciók");
                Console.WriteLine("Mit szeretne végrehajtani?(1,2,3,4,5)");
                Console.WriteLine();

                string key = BusinessLogic.rightNumberCheck(1,5);
                
                switch (int.Parse(key))
                {
                    case 1:
                        Console.Clear();

                        Console.WriteLine("         Listázás:");
                        logic.List();
                        break;
                    case 2:
                        Console.Clear();

                        Console.WriteLine("         Módosítás:");
                        logic.Modify();
                        break;
                    case 3:
                        Console.Clear();

                        Console.WriteLine("         Törlés:");
                        logic.Delete();
                        break;
                    case 4:
                        Console.Clear();

                        Console.WriteLine("         Hozzáadás:");
                        logic.Add();
                        break;
                    case 5:
                        Console.Clear();

                        Console.WriteLine("         Extrák:");
                        logic.Extras();
                        break;
                    default:
                        Console.WriteLine("Hiba");
                        break;
                }

                Console.WriteLine("Ki szeretnél lépni?(i-n)");
                
                string continueRunnin=Console.ReadLine();
                if (continueRunnin!="n")
                {
                    programRunnin = false;
                }

            } while (programRunnin);
        }
    }
}
