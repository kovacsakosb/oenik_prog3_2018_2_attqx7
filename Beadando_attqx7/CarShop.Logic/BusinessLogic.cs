﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CarShop.Data;
using CarShop.Repository;


namespace CarShop.Logic
{
    public class BusinessLogic:IBusinessLogic
    {
        CarRepository repository;

        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="database">The sql database</param>
        public BusinessLogic(CarShopEntities database)
        {
            repository = new  CarRepository(database);
        }
        
        /// <summary>
        /// List method, list all record from the choosen table.
        /// </summary>
        public void List()
        {
            Console.Clear();
            Console.WriteLine("             Listázás            ");
            Console.WriteLine("Lehetőségek: ");
            Console.WriteLine(" [1], Automárkák");
            Console.WriteLine(" [2], Extrák");
            Console.WriteLine(" [3], Modellek");
            Console.WriteLine("Mit szeretne listázni?(1,2,3)");
            Console.WriteLine();
            string key = rightNumberCheck(1,3);

            switch (int.Parse(key))
            {
                case 1:
                    Console.Clear();

                    Console.WriteLine("         Automárkák:");
                    AutoMarkak auto = new AutoMarkak();
                    Console.WriteLine(  repository.List(auto));
                    break;
                case 2:
                    Console.Clear();

                    Console.WriteLine("         Extrák:");
                    Extrak extra = new Extrak();
                    Console.WriteLine(repository.List(extra));
                    break;
                case 3:
                    Console.Clear();

                    Console.WriteLine("         Modellek:");
                    Modellek modell = new Modellek();
                    Console.WriteLine(repository.List(modell));
                    break;
                default:
                    Console.WriteLine("Hiba");
                    break;
            }

        }
        /// <summary>
        /// Get a number from console between min and max while the value is not correct(max 10 iteration).
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        /// <returns></returns>
        public static string rightNumberCheck(int min,int max)
        {
            string key = Console.ReadLine();
            bool rightinput = (Regex.IsMatch(key, @"^[0-9]+$") && int.Parse(key) >= min && int.Parse(key) <= max);
            int infinityDodge = 0;
            while (!rightinput && infinityDodge < 10)
            {
                Console.WriteLine("Helytelen bevitel, adjon meg egy újat:");
                key = Console.ReadLine();
                rightinput = (Regex.IsMatch(key, @"^[0-9]+$") && int.Parse(key) >= min && int.Parse(key) <= max);
                infinityDodge++;
            }

            return key;
        }
        /// <summary>
        /// Add an entity to the choosen table with the correct datas
        /// </summary>
        public void Add()
        {
            Console.Clear();
            Console.WriteLine("             Hozzáadás            ");
            Console.WriteLine("Lehetőségek: ");
            Console.WriteLine(" [1], Automárkák");
            Console.WriteLine(" [2], Extrák");
            Console.WriteLine(" [3], Modellek");
            Console.WriteLine("Mit szeretne hozzáadni?(1,2,3)");
            Console.WriteLine();

            string key=rightNumberCheck(1, 3);

            switch (int.Parse(key))
            {
                case 1:
                    Console.Clear();

                    Console.WriteLine("         Automárkák:");
                    AutoMarkak auto = new AutoMarkak();
                    Console.WriteLine("ID:");
                    string id = rightNumberCheck(0,int.MaxValue);
                    auto.Id = int.Parse(id);
                    Console.WriteLine("Név:");
                    auto.nev = Console.ReadLine();
                    Console.WriteLine("Ország név:");
                    auto.Orszag_nev = Console.ReadLine();
                    Console.WriteLine("Url:");
                    auto.url = Console.ReadLine();
                    Console.WriteLine("Alapítás éve:");
                    string alapitas = rightNumberCheck(0, int.MaxValue);
                    auto.Alapitas_eve = int.Parse(alapitas);
                    Console.WriteLine("Éves forgalom:");
                    string forgalom = rightNumberCheck(0, int.MaxValue);
                    auto.Eves_forgalom = int.Parse(forgalom);
                    repository.Add(auto);
                    break;
                case 2:
                    Console.Clear();

                    Console.WriteLine("         Extrák:");
                    Extrak extrak = new Extrak();
                    Console.WriteLine("ID:");
                    string ide = rightNumberCheck(0, int.MaxValue);
                    extrak.Id = int.Parse(ide);
                    Console.WriteLine("Kategóri név:");
                    extrak.Kategoria_nev = Console.ReadLine();
                    Console.WriteLine("Név:");
                    extrak.Nev = Console.ReadLine();
                    Console.WriteLine("Ár:");
                    string are = rightNumberCheck(0, int.MaxValue);
                    extrak.Ar = int.Parse(are);
                    Console.WriteLine("Szín:");
                    extrak.Szin = Console.ReadLine();
                    Console.WriteLine("Garancia(0/1):");
                    string gari = rightNumberCheck(0, 1);
                    extrak.Garancia = bool.Parse(gari);
                    repository.Add(extrak);
                    break;
                case 3:
                    Console.Clear();

                    Console.WriteLine("         Modellek:");
                    Modellek modell = new Modellek();
                    Console.WriteLine("ID:");
                    string idm = rightNumberCheck(0, int.MaxValue);
                    modell.Id = int.Parse(idm);
                    Console.WriteLine("Márka Id:");
                    string idmm = rightNumberCheck(0, int.MaxValue);
                    modell.Marka_Id = int.Parse(idmm);
                    Console.WriteLine("Név:");
                    modell.Nev = Console.ReadLine();
                    Console.WriteLine("Megjelenés éve:");
                    string megj = rightNumberCheck(0, int.MaxValue);
                    modell.Megjelenes_eve = int.Parse(megj);
                    Console.WriteLine("Motortérfogat:");
                    string motor = rightNumberCheck(0, int.MaxValue);
                    modell.Motorterfogat = int.Parse(motor);
                    Console.WriteLine("Lóerő:");
                    string loers = rightNumberCheck(0, int.MaxValue);
                    modell.Loero = int.Parse(loers);
                    Console.WriteLine("Alapár:");
                    string alapar = rightNumberCheck(0, int.MaxValue);
                    modell.Alapar = int.Parse(alapar);
                    repository.Add(modell);
                    break;
                default:
                    Console.WriteLine("Hiba");
                    break;
            }
        }
        /// <summary>
        /// Delete an entity from the choosen table by id
        /// </summary>
        public void Delete()
        {
            Console.Clear();
            Console.WriteLine("             Törlés            ");
            Console.WriteLine("Lehetőségek: ");
            Console.WriteLine(" [1], Automárkák");
            Console.WriteLine(" [2], Extrák");
            Console.WriteLine(" [3], Modellek");
            Console.WriteLine("Mit szeretne törölni?(1,2,3)");
            Console.WriteLine();

            string key = rightNumberCheck(1, 3);

            switch (int.Parse(key))
            {
                case 1:
                    Console.Clear();

                    Console.WriteLine("         Automárkák:");
                    AutoMarkak auto = new AutoMarkak();
                    Console.WriteLine(   repository.List(auto));
                    Console.WriteLine("Mit szeretne törölni? (Id-t adja meg)");
                    string deletecarid = rightNumberCheck(0,int.MaxValue);
                    repository.DeleteAuto(int.Parse(deletecarid));
                    Console.WriteLine(   repository.List(auto));
                    break;
                case 2:
                    Console.Clear();

                    Console.WriteLine("         Extrák:");
                    Extrak extra = new Extrak();
                    Console.WriteLine(   repository.List(extra));
                    Console.WriteLine("Mit szeretne törölni? (Id-t adja meg)");
                    string deleteexid = rightNumberCheck(0, int.MaxValue);
                    repository.DeleteExtra(int.Parse(deleteexid));
                    Console.WriteLine(   repository.List(extra));
                    break;
                case 3:
                    Console.Clear();

                    Console.WriteLine("         Modellek:");
                    Modellek modell = new Modellek();
                    Console.WriteLine(   repository.List(modell));
                    Console.WriteLine("Mit szeretne törölni? (Id-t adja meg)");
                    string deletemodid = rightNumberCheck(0, int.MaxValue);
                    repository.DeleteModel(int.Parse(deletemodid));
                    Console.WriteLine(   repository.List(modell));
                    break;
                default:
                    Console.WriteLine("Hiba");
                    break;
            }
        }
        /// <summary>
        /// modify an entity choosen value from a choosen table 
        /// </summary>
        public void Modify()
        {
            Console.Clear();
            Console.WriteLine("             Módosítás            ");
            Console.WriteLine("Lehetőségek: ");
            Console.WriteLine(" [1], Automárkák");
            Console.WriteLine(" [2], Extrák");
            Console.WriteLine(" [3], Modellek");
            Console.WriteLine("Mit szeretne módosítani?(1,2,3)");
            Console.WriteLine();

            string key = rightNumberCheck(1,3);
            int id, propertyNumber;
            string newValue;
            switch (int.Parse(key))
            {
                case 1:
                    Console.Clear();

                    Console.WriteLine("         Automárkák:");
                    AutoMarkak auto = new AutoMarkak();
                    Console.WriteLine(   repository.List(auto));
                    Console.WriteLine("Mit szeretne módosítani? (Id-t adja meg)");
                    string automod = rightNumberCheck(0, int.MaxValue);
                    id = int.Parse(automod);
                    Console.WriteLine("Adja meg hanyadik tulajdonságot szeretné módosítani:");
                    Console.WriteLine("[2]Név [3]Ország Név [4]Url [5]Alapítás éve [6]Éves forgalom");
                    string automodmod = rightNumberCheck(2,6);
                    propertyNumber = int.Parse(automodmod);
                    Console.WriteLine("Adja meg az új értéket:");
                    newValue = Console.ReadLine();
                    repository.UpdateAuto(id,propertyNumber,newValue);
                    Console.WriteLine(   repository.List(auto));

                    break;
                case 2:
                    Console.Clear();

                    Console.WriteLine("         Extrák:");
                    Extrak extra = new Extrak();
                    Console.WriteLine(   repository.List(extra));
                    Console.WriteLine("Mit szeretne módosítani? (Id-t adja meg)");
                    string extramod = rightNumberCheck(0, int.MaxValue);
                    id = int.Parse(extramod);
                    Console.WriteLine("Adja meg hanyadik tulajdonságot szeretné módosítani:");
                    Console.WriteLine("[2]Kategória név [3]Név [4]Ár [5]Szín [6]Garancia");
                    string extramodmod = rightNumberCheck(2, 6);
                    propertyNumber = int.Parse(extramodmod);
                    Console.WriteLine("Adja meg az új értéket:");
                    newValue = Console.ReadLine();
                    repository.UpdateExtra(id, propertyNumber, newValue);
                    Console.WriteLine(   repository.List(extra));
                    break;
                case 3:
                    Console.Clear();

                    Console.WriteLine("         Modellek:");
                    Modellek modell = new Modellek();
                    Console.WriteLine(   repository.List(modell));
                    Console.WriteLine("Mit szeretne módosítani? (Id-t adja meg)");
                    string modelmod = rightNumberCheck(0, int.MaxValue);
                    id = int.Parse(modelmod);
                    Console.WriteLine("Adja meg hanyadik tulajdonságot szeretné módosítani:");
                    Console.WriteLine("[2]Márka id [3]Név [4]Megjelenés éve [5]Motortérfogat [6]Lóerő [7]Alapár");
                    string modelmodmod = rightNumberCheck(2, 7);
                    propertyNumber = int.Parse(modelmodmod);
                    Console.WriteLine("Adja meg az új értéket:");
                    newValue = Console.ReadLine();
                    repository.UpdateModel(id, propertyNumber, newValue);
                    Console.WriteLine(   repository.List(modell));

                    break;
                default:
                    Console.WriteLine("Hiba");
                    break;
            }
        }
        /// <summary>
        /// call 3 extra method ,1; list cars by horsepower, 2; list cars by extra, 3; list a brand each modell
        /// </summary>
        public void Extras()
        {
            Console.WriteLine("Adja meg mit szeretne végrehajtani:");
            Console.WriteLine("[1]Lóerő alapján  szűrés");
            Console.WriteLine("[2]Extrák alapján szűrés");
            Console.WriteLine("[3]Egy márka modelljei listázása");
            int funct =int.Parse(rightNumberCheck(1,3));
            switch (funct)
            {
                case 1:
                    Console.Clear();
                    Console.WriteLine("Adja meg milyen erős autót szeretne legalább:");
                    string hrose = rightNumberCheck(0, int.MaxValue);
                    int horse = int.Parse(hrose);
                    Console.WriteLine(repository.ListHorse(horse));

                    break;
                case 2:
                    Console.Clear();
                    Console.WriteLine("Adja meg milyen extrákat szeretne az autóba:");
                    string extras = Console.ReadLine();
                    Console.WriteLine(repository.ExtraFilter(extras));
                    break;
                case 3:
                    Console.Clear();
                    Console.WriteLine("Adja meg milyen márkát listázna:");
                    string marka = Console.ReadLine();
                    Console.WriteLine(repository.BrandList(marka));
                    break;
                default:
                    Console.WriteLine("Hiba");
                    break;
            }
           
        }
    }
    public interface IBusinessLogic
    {
        void List();
        //string rightNumberCheck(int min, int max);
        void Add();
        void Delete();
        void Modify();
        void Extras();
    }
}
